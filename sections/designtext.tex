\section{Text Detection}

Text detection on the paragraph level distinguishes texts from images and
generate bounding boxes for each paragraph. Different from optical character
recognition (OCR) techniques, text detection doesn't recognize every word in
the input image. Although there have been many studies on OCR and existing OCR
engines have been proven to be powerful and accurate in many cases, we argue
that our text detection approach is more suitable than existing solid,
open-sourced OCR engines like Tesseract\cite{smith2007overview} for our work
because:

\begin{itemize}

    \item we don't need character/word prediction nor their localization
        information. It is hard to transform the output of the OCR engine to
        fit our need (paragraph localization information).

    \item OCR engines may not be language-independent.  For example, Tesseract
        requires each language having one specific trained model to perform
        better, and the preparation can be time-consuming and not applicable if
        we don't have any assumption about the language shown on the input
        image.

    \item as discussed in \cite{shah2011reverse} and \cite{karatzas2013icdar},
        OCR would have poor results if text is arranged freely and combined
        with images, which is typically how user interfaces are designed. Our
        preliminary tests show that OCR tools fail to have comparable
        performace on the desktop screen, indicating that using these OCR tools
        will reduce stability of our algorithm. This is the most important
        reason why we develop our own model.

\end{itemize}

Based on the discussion above, we leverage the off-the-shelf object detection
model Faster R-CNN\cite{ren2015faster} for our work. The implementation is
modified from an online, open-sourced
version\footnote{https://github.com/chenyuntc/simple-faster-rcnn-pytorch/}
rather than the official one because it can be integrated into our codebase
more easily. This model takes a 512x512, normalized RGB image as input and
return $(X_{\text{min}}, Y_{\text{min}}, X_{\text{max}}, Y_{\text{max}}, confidence)$
tuples for each paragraph it finds.

To train the model, we collect 3867 images from Alexa's top one million website
list using browser automation technology. For each website, only one screenshot
is captured either from the main page or a page selected by randomly clicking
a link on the main page. This is to avoid any potential design bias between the
main page and other pages.  Paragraph localization labels are also collected
when visiting the website.  Then, these images are cropped into required
dimension and fed into the model for training after standard data augmentation
steps like random horizontal flipping, random scaling, and normalization.

We find out that using deep learning models not only avoids the drawbacks
mentioned above, but has the following advantages:

\begin{itemize}

    \item \textit{Time efficiency.} While inference speed varies by the actual
        implementation, the running time of deep learning models is generally
        smaller than that of OCR engines.  This becomes more prominent when the
        input comes from a long, scrollable web page, which can be commonly
        seen in modern websites.

    \item \textit{Generalization.} The model proves to be very robust,
        especially on the aspect of language and user interface environment.
        Although the training data come from website screenshots and most of
        display languages in the dataset is English, it performs well in other
        cases.  Moreover, the accuracy can be improved by collecting more data
        from different sources without modifying the model.

\end{itemize}

The output tuples with confidence score greater than 0.7 are put into the
computer vision process.
